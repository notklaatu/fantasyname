# d1100 Fantasy names

1100 Generic fantasy names in 10 tables of 100 (don't check my maths).

This is a collection of 10 tables of unsorted fantasy names. They have no categories (like Orc, Elf, Dwarf, Human, and so on), and instead provide random, generic names to serve, if nothing else, a basis for a name of your own creation.

Roll once to determine what table to roll against.

Roll once and only once to get your name.

Roll only once, each time. If you keep rolling until you hit a name you like, the tables lose their magic. That's the rule.

If you roll a name you dislike, use it as the basis for a new name (Blossom becomes Blossovar, Blasskrul, or Bloodflower the Unquenchable). Or try adding -kar or -on to the end of a name you don't like. Use your imagination!


## Building from source

You don't need to build these tables from source code. They are available for free from [dmsguild.com](http://www.dmsguild.com/product/235759/Census-of-the-realms-d1100-fantasy-names)

If you want to, or need to, build from source for reasons all your own, then here is how.

This has only been tested on Linux and BSD, but as long as you know Docbook, you should be able to use this with only modest changes to the GNUmakefile.
      
*Assuming you're using Linux or BSD, the required software is probably available from your software repository or ports tree.*

Requirements:

* [Docbook](http://docbook.org)
* [Junction](https://www.theleagueofmoveabletype.com/junction) font
* [Andada](http://www.1001fonts.com/andada-font.html)
* [TeX Gyre Bonum](http://www.1001fonts.com/tex-gyre-bonum-font.html) (bundled in this repo to fix render errors)
* [xmlto](https://pagure.io/xmlto)
* [xsltproc](http://xmlsoft.org/XSLT/index.html)
* [FOP 2](https://xmlgraphics.apache.org/fop/)
* [pdftk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/) or [stapler](https://pypi.python.org/pypi/stapler)
* [Image Magick](http://imagemagick.org/script/index.php)

Optional, to avoid warnings about not having a **symbol** font installed:

* [UniCons](https://fontlibrary.org/en/font/unicons)


### Setup

The font **TeX Gyre Bonum** does not function properly because it's OTF rather than TTF.

I have converted the source OTF to TTF for you. They're included with this repository.

Place them in your ``$HOME/.local/share/fonts/t/`` directory so Docbook can detect them.


### GNUmakefile

The GNUmakefile builds a PDF, the standard delivery format for most indie RPG utilities. It also builds EPUB, the *better* delivery format for indie RPG utilities.

For PDF, you can edit inline parameters to suit your needs.

Notably:

* paper size is A4 by default
* license is set to ``no`` by default, which renders the standard *for personal use only* footer. You can alternately set it to ``sa`` for a Creative Commons BY-SA license message in the footer, instead. The Open Game License only covers mechanics, so your CC license only applies to your *story content*
* the name of the output file is ``example.pdf`` by default

To build:

    $ make clean pdf

For EPUB, style changes are contained in ``style/style.css``.

To build:

    $ make clean epub
    
The output is placed into a directory called ``dist``.



## Bugs

Report any other bugs you find, or even feedback or suggestions, in the issues tab on Gitlab. Alternately, email klaatu at the domain member.fsf.org


## Contribute

Yes, you can contribute. I do have some ugly hacks in here (for instance, I'm re-purposing ``informalexample`` for the singular purpsoe of centering the dmsguild logo on the title page), so if you know XSL or XML, please make improvements and send them my way. I'm happy to incorporate improvements.

## License

It's all open source and free culture. See LICENSE file for details.